//version inicial

var express = require('express'),
 app = express(),
 port = process.env.PORT || 3000;

var path = require('path');
var movimientosJSON = require('./movimientosv2.json');
var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
 res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function(req, res){
 res.send('petición post cambiada');  
})

app.put('/', function(req, res) {
 res.send('petición put NodeJS');
})

app.delete('/', function(req, res){
 res.send('Hola recibido su petición delete');
})

app.get('/Clientes/:idcliente', function(req, res){
 res.send('Aqui tiene al cliente número ' + req.params.idcliente);
})

app.get('/Movimientos', function(req, res){
 res.sendfile('movimientosv1.json');
})


app.get('/V2/Movimientos', function(req, res){
 res.json(movimientosJSON);
})

app.get('/V2/Movimientos/:id', function(req, res){
  console.log(req.params.id);
 res.send(movimientosJSON[req.params.id-1]);
})

app.get('/V2/MovimientosQ', function(req, res){
  console.log(req.query);
 res.send('Query recibido ' + req.query);
})

app.post('/V2/Movimientos', function(req, res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
 res.send('Movimienot dado de alta ');
})
